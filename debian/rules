#!/usr/bin/make -f

export DEB_BUILD_MAINT_OPTIONS = hardening=+all optimize=+lto
export DEB_LDFLAGS_MAINT_APPEND = -Wl,-z,noexecstack

# disable hardening-pie on x32
# fixes compilation problem: relocation R_X86_64_PC32 against symbol `cpucycles'
# can not be used when making a shared object
ifeq ($(DEB_BUILD_ARCH), x32)
export DEB_BUILD_MAINT_OPTIONS = hardening=+all,-pie optimize=+lto
endif

# disable hardening-pie on ia64
# fixes compilation problem: @gprel relocation against dynamic symbol cpucycles
ifeq ($(DEB_BUILD_ARCH), ia64)
export DEB_BUILD_MAINT_OPTIONS = hardening=+all,-pie optimize=+lto
endif

# disable hardening-pie on alpha
# fixes compilation problem: gp-relative relocation against dynamic symbol cpucycles
ifeq ($(DEB_BUILD_ARCH), alpha)
export DEB_BUILD_MAINT_OPTIONS = hardening=+all,-pie optimize=+lto
endif


%:
	dh $@

# In the debian/compiler directory contains gcc and clang wrappers.
# It adds Debian specifific options distributed in
# $CC, $CFLAGS, $CPPFLAGS and $LDFLAGS
export PATH := $(shell pwd)/debian/compiler:$(PATH)

# Detects the correct ABINAME from the DEB_BUILD_ARCH
# instead of run-time detection. Fixes FTBR.
ABINAME=$(DEB_BUILD_ARCH)
ifeq ($(ABINAME), x32)
ABINAME=amd64
endif

override_dh_auto_configure:
	echo "debug: debian adds these compiler options: ${CFLAGS} ${CPPFLAGS} ${LDFLAGS}"
	./configure --host=$(ABINAME) --prefix=/usr

override_dh_auto_clean:
	rm -rf ./build

# disable auto installation
override_dh_auto_install:
